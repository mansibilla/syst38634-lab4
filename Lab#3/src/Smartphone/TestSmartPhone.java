package Smartphone;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSmartPhone {

	SmartPhone smartPhone = new SmartPhone();

	@Test(expected = VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {
		double version = 9.0;
		smartPhone.setVersion(version);
	}

	@Test
	public void testSetVersionMin() throws VersionNumberException {
		double version = 3.0;
		smartPhone.setVersion(version);
		assertTrue(smartPhone.getVersion() == version);

	}

	@Test
	public void testSetVersion() throws VersionNumberException {
		double version = 4.0;
		smartPhone.setVersion(version);
		assertTrue(smartPhone.getVersion() == version);

	}
}
