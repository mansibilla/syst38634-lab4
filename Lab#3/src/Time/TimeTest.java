/**
 * 
 */
package Time;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class TimeTest {
	
	Time timeVar = new Time();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}


	@Before
	public void setUp() throws Exception {
	}

	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetTotalSeconds() {
		timeVar.getTotalSeconds("12:05:05");
		assertTrue(timeVar.getTotalSeconds("12:05:05") == 43505);
		
	}
	
	@Test(expected = StringIndexOutOfBoundsException.class)
	public void testGetTotalSecondsException() throws StringIndexOutOfBoundsException
	{
		timeVar.getTotalSeconds("0");
		
	}

	@Test
	public  void testGetMilliSeconds() 
	{
		timeVar.getMilliSeconds("12:05:05:05");
		assertTrue(timeVar.getMilliSeconds("12:05:05:05") == 05);
		
	}


	
	
}
