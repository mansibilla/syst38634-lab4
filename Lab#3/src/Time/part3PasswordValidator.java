package Time;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class part3PasswordValidator {
	
	public static void main(String[] args) 
	{
		
	}
	 
	    private static Pattern pattern;
	    private static Matcher matcher;
	 
	    private static final String PASSWORD_PATTERN = "^\\D*(\\d)\\D*(\\d)\\D*$*";
	 
	    public part3PasswordValidator() {
	        pattern = Pattern.compile(PASSWORD_PATTERN);
	    }
	 
	    public static boolean validate(final String password) {
	 
	    	if(password.length() >= 8) {
	        matcher = pattern.matcher(password);
	        return matcher.matches();
	 
	    	}
	    	return false;
	    }
	}