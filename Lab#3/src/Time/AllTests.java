package Time;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({})
public class AllTests {

	@Test
	public void testGetTotalSeconds() {
		int seconds = Time.getTotalSeconds("12:05:05"); 
		assertTrue("The seconds were not calculated properly", seconds == 43505);
		
	}
	
}
